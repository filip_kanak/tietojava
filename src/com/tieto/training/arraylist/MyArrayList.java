package com.tieto.training.arraylist;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public class MyArrayList<E> implements List<E> {

	private int size;
	private Object p[];
	private IteratorMethod lastCalled = IteratorMethod.NOTHING;

	public MyArrayList() {
		p = new Object[16];
		size = 0;
	}

	private enum IteratorMethod {

		NOTHING, PREVIOUS, NEXT
	};

	@Override
	public boolean add(E e) {
		if ((size + 1) > p.length) {
			Object[] data = p;
			p = new Object[p.length + 16];
			for (int j = 0; j < data.length; j++) {
				p[j] = data[j];
			}
		}

		p[size] = e;
		size++;
		return true;
	}

	@Override
	public void add(int index, E e) {
		if ((size + 1) > p.length) {
			Object[] data = p;
			p = new Object[p.length + 16];
			for (int j = 0; j < data.length; j++) {
				p[j] = data[j];
			}
		}
		for (int i = size; i >= index; i--) {
			p[i + 1] = p[i];
		}
		p[index] = e;
		size++;

	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		p = new Object[16];
		size = 0;
	}

	@Override
	public boolean contains(Object o) {
		for (int i = 0; i < p.length; i++) {
			if (Objects.equals(p[i], o))
				return true;
		}
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean equals(Object o) {
		if (o == this)
            return true;
        if (!(o instanceof List))
            return false;

        ListIterator<E> e1 = listIterator();
        ListIterator<?> e2 = ((List<?>) o).listIterator();
        while (e1.hasNext() && e2.hasNext()) {
            E o1 = e1.next();
            Object o2 = e2.next();
            if (!(o1==null ? o2==null : o1.equals(o2)))
                return false;
        }
        return !(e1.hasNext() || e2.hasNext());
	}

	@SuppressWarnings("unchecked")
	@Override
	public E get(int index) {
		return (E) p[index];
	}

	@Override
	public int indexOf(Object o) {
		for (int i = 0; i < p.length; i++) {
			if (p[i] == o)
				return i;
		}
		return -1;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public Iterator<E> iterator() {
		Iterator<E> it = new MyArrayListIterator();
		return it;
	}

	public class MyArrayListIterator implements ListIterator<E> {
		private int position = 0;

		public boolean hasNext() {
			return position < size;
		}

		public E next() {
			if (!hasNext())
				throw new NoSuchElementException();
			lastCalled = IteratorMethod.NEXT;
			return (E) get(position++);
		}

		@Override
		public boolean hasPrevious() {
			return position > 0;
		}

		@Override
		public E previous() {
			if (!hasPrevious())
				throw new NoSuchElementException();
			lastCalled = IteratorMethod.PREVIOUS;
			return (E) get(--position);
		}

		@Override
		public int nextIndex() {
			return position;
		}

		@Override
		public int previousIndex() {
			return position - 1;
		}

		@Override
		public void remove() {
			if (lastCalled == IteratorMethod.NOTHING) {
				throw new IllegalStateException();
			}
			if (lastCalled == IteratorMethod.PREVIOUS) {
				MyArrayList.this.remove(position);
			}
			if (lastCalled == IteratorMethod.NEXT) {
				MyArrayList.this.remove(position - 1);
			}
			lastCalled = IteratorMethod.NOTHING;
		}

		@Override
		public void set(E e) {
			if (lastCalled == IteratorMethod.NOTHING) {
				throw new IllegalStateException();
			}
			if (lastCalled == IteratorMethod.PREVIOUS) {
				MyArrayList.this.set(position, e);
			}
			if (lastCalled == IteratorMethod.NEXT) {
				MyArrayList.this.set(position - 1, e);
			}
			lastCalled = IteratorMethod.NOTHING;
		}

		@Override
		public void add(E e) {
			MyArrayList.this.add(position, e);
			position++;
			lastCalled = IteratorMethod.NOTHING;
		}

	}

	@Override
	public int lastIndexOf(Object o) {
		for (int i = p.length - 1; i >= 0; i--) {
			if (Objects.equals(p[i], o))
				return i;
		}
		return -1;
	}

	@Override
	public ListIterator<E> listIterator() {
		ListIterator<E> it = new MyArrayListIterator();
		return it;
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		int index = -1;
		for (int i = 0; i < p.length; i++) {
			if (p[i] == o) {
				p[i] = null;
				index = i;
				break;
			}
		}
		if (index == -1)
			return false;
		for (int j = index; j < size; j++) {
			p[j] = p[j + 1];
		}
		size--;
		return true;
	}

	@Override
	public E remove(int index) {
		if (index < 0 || index >= size)
			throw new IndexOutOfBoundsException();
		@SuppressWarnings("unchecked")
		E toreturn = (E) p[index];
		p[index] = null;

		for (int j = index; j < size; j++) {
			p[j] = p[j + 1];
		}
		size--;
		return toreturn;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E set(int index, E element) {
		@SuppressWarnings("unchecked")
		E toreturn = (E) p[index];
		p[index] = element;
		return toreturn;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public List<E> subList(int fromIndex, int toIndex) {

		if (fromIndex < 0 || toIndex > size || fromIndex > toIndex) {
			throw new IllegalArgumentException("fromIndex(" + fromIndex + ")>toIndex(" + toIndex + ")");
		}
		return new SubList(fromIndex, toIndex);
	}

	enum LastUsedSublist {
		NEXT, PREVIOUS, REMOVE, SET, ADD
	}

	class SubList implements List<E> {

		int fromIndex;
		int toIndex;
		LastUsedSublist lastUsedSublist;

		public SubList(int fromIndex, int toIndex) {
			this.fromIndex = fromIndex;
			this.toIndex = toIndex;
		}

		@Override
		public int size() {

			return toIndex - fromIndex;
		}

		@Override
		public boolean isEmpty() {

			return (size() == 0);
		}

		@Override
		public boolean contains(Object o) {
			for (int i = fromIndex; i < toIndex; i++) {
				if (p[i] == o)
					return true;
			}
			return false;
		}

		@Override
		public Iterator<E> iterator() {
			 Iterator<E> it = new MySubListIterator();
			return it;
		}
		private class MySubListIterator implements Iterator<E> {
			int position = 0;

			@Override
			public boolean hasNext() {
				return (position < toIndex);
			}

			@Override
			public E next() {
				return get(position++);
			}
		}

		@Override
		public ListIterator<E> listIterator() {
			ListIterator<E> it = new MySubListListIterator();
			return it;
		}

		private class MySubListListIterator implements ListIterator<E> {
			private int position = 0;

			@Override
			public void add(E e) {
				MyArrayList.this.add(position+fromIndex, e);
				lastUsedSublist = LastUsedSublist.ADD;

			}

			@Override
			public boolean hasNext() {
				return position < toIndex;
			}

			@Override
			public boolean hasPrevious() {
				return fromIndex < position;
			}

			@Override
			public E next() {
				if (!(hasNext())) {
					throw new NoSuchElementException();
				}
				lastUsedSublist = LastUsedSublist.NEXT;
				return get(position++);
			}

			@Override
			public int nextIndex() {
				if (position == size) {
					return size;
				}
				return position;
			}

			@Override
			public E previous() {
				if (!(hasPrevious())) {
					throw new NoSuchElementException();
				}
				lastUsedSublist = LastUsedSublist.PREVIOUS;
				return get(position--);
			}

			@Override
			public int previousIndex() {
				if (position == 0) {
					return -1;// just to be sure
				}
				return position - 1;
			}

			@Override
			public void remove() {
				if (lastUsedSublist == LastUsedSublist.NEXT || lastUsedSublist == LastUsedSublist.PREVIOUS) {
					MyArrayList.this.remove(position - 1);
					lastUsedSublist = LastUsedSublist.REMOVE;
					toIndex--;
					return;
				}
				throw new IllegalStateException();

			}

			@Override
			public void set(E e) {
				if (lastUsedSublist == LastUsedSublist.NEXT || lastUsedSublist == LastUsedSublist.PREVIOUS){
					MyArrayList.this.set(position - 1, e);
					lastUsedSublist = LastUsedSublist.SET;
					return;
				}
				throw new IllegalStateException();

			}

		}

		@Override
		public Object[] toArray() {
			// TODO Auto-generated method stub
			return null;
		}

		@SuppressWarnings("unchecked")
		@Override
		public Object[] toArray(Object[] a) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean add(E e) {
			MyArrayList.this.add(toIndex, e);
			return true;
		}

		@Override
		public boolean remove(Object o) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean containsAll(Collection<?> c) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean addAll(Collection<? extends E> c) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean addAll(int index, Collection<? extends E> c) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean removeAll(Collection<?> c) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean retainAll(Collection<?> c) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void clear() {
			int subListSize = size();
			for (int i = fromIndex; i < subListSize + fromIndex; i++) {
				p[i] = p[i + subListSize];
			}
			toIndex = fromIndex;
			size -= subListSize;
		}

		@Override
		public E get(int index) {
			return MyArrayList.this.get(index + fromIndex);
		}

		@Override
		public E set(int index, E element) {
			return MyArrayList.this.set(index + fromIndex, element);
		}

		@Override
		public void add(int index, E element) {
			MyArrayList.this.add(fromIndex + index, element);

		}

		@Override
		public E remove(int index) {
			E element = MyArrayList.this.get(index + fromIndex);
			MyArrayList.this.remove(index + fromIndex);
			toIndex--;
			return element;
		}

		@Override
		public int indexOf(Object o) {

			return MyArrayList.this.indexOf(o) + fromIndex;
		}

		@Override
		public int lastIndexOf(Object o) {

			return MyArrayList.this.lastIndexOf(o) + fromIndex;
		}



		@Override
		public ListIterator<E> listIterator(int index) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<E> subList(int fromIndex, int toIndex) {
			if (fromIndex < 0 || toIndex > size || fromIndex > toIndex) {
				throw new IllegalArgumentException(/*
													 * "fromIndex("+fromIndex+
													 * ")>toIndex("+toIndex+")"
													 */);
			}
			return MyArrayList.this.subList(this.fromIndex + fromIndex, this.toIndex + toIndex);
		}

	}

	@Override
	public Object[] toArray() {
		Object arrayToReturn[] = new Object[size];

		for (int j = 0; j < size; j++) {
			arrayToReturn[j] = p[j];
		}
		return arrayToReturn;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

}
