package com.tieto.training.lesson1;

/**
 * Represent an animal
 * @return
 * */
public interface Animal {
  /**
   * Get the name of the animal species
   * @return
   */
	String getSpecies();
	
	/**
	 * Get the typical sound the animal produces
	 * 
	 */
	String getSound();
	/**
	 * 
	 * Get the normal number of legs
	 * @return
	 */
	int getNrOfLegs();
}
