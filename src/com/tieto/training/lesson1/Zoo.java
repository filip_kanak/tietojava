package com.tieto.training.lesson1;

public class Zoo {
	public void presentAllAnimals(Animal[] beasts)
		{
		for (int i = 0; i < beasts.length; i++) {
		final Animal a = beasts[i];
		System.out.println(a.getSpecies()+": "+a.getSound());
		}
	}
}
