package com.tieto.training.lesson1;

public class ZooLauncher {
	
	public static void main(String[] args)
	{
	 final Animal[] animals = new Animal[4];
     animals[0] = new Dog();
     animals[1] = new Cat();
     animals[2] = new Whale();
     animals[3] = new Cow();
     
     
     final Zoo zoo = new Zoo();
     zoo.presentAllAnimals(animals);
}
}