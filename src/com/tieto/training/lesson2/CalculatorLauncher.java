package com.tieto.training.lesson2;

public class CalculatorLauncher {
   public static void main(String[] args)
   {
	   final Calculator calc = new Calculator();
	  final double hypotenuse = calc.hypotenuse(4.0, 10.0);
	  System.out.println(hypotenuse);
   }
}
