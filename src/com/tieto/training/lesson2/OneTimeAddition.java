package com.tieto.training.lesson2;

public class OneTimeAddition {
		final private int valueToAdd;
		private boolean alreadyAdded;
		
		public OneTimeAddition(int valueToAdd)
		{this.valueToAdd = valueToAdd;
		this.alreadyAdded = false;}
		
		public int add(int a)
		{if(alreadyAdded){throw new IllegalStateException();}
		alreadyAdded = true;	
		return a + valueToAdd;}
}
