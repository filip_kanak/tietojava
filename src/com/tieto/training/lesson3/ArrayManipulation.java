package com.tieto.training.lesson3;

import java.util.Comparator;

public class ArrayManipulation {
	/**
	 * Return the biggest element of the given array.
	 * @param input An input array of numbers. Null is handled as an empty array.
	 * The result of the method is not defined if case is not empty array.
	 * If the array is empty, returns Integer.MIN_VALUE
	 * @return
	 */
	public int findBiggestElement(int[] input)
	{
		if(input == null)
			{
			return Integer.MIN_VALUE;
			}
		
		int biggest=Integer.MIN_VALUE;
		
		for(int i=0; i < input.length ;i++)
		{
			if(input[i] > biggest)biggest=input[i];
		}
		
		return biggest;
	}
	/**
	 * Return the biggest element of the given array.
	 * @param <E>
	 * @param input An input array of numbers. Null is handled as an empty array.
	 * The result of the method is not defined if case is not empty array.
	 * 
	 * @return
	 * If the array is empty, returns empty String
	 */
	public <E> E findBiggestElement(E[] input, Comparator<E> comparator)
	{
		if(input == null)
			{
			return null;
			}
		
		E biggest=null;
		
		for(int i=0; i < input.length ;i++)
		{
			if(comparator.compare(input[i],biggest)>0)biggest=input[i];
		}
		
		return biggest;
	}
	
	
}


