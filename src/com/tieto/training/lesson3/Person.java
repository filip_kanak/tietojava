package com.tieto.training.lesson3;

import java.util.Comparator;
import java.util.Date;


public class Person implements Comparable<Person>{
	final private String firstName;
	final private String lastName;
	final private Date birthDate;
	
	public Person(String firstName, String lastName, Date birthDate)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Date getBirthDate() {
		return birthDate;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		return true;
	}
	public static Person findBiggestElement(Person[] input,ComparatorPerson comparator)
	{
		if(input == null)
		{
		return null;
		}
	
	Person biggest=null;
	
	for(int i=0; i < input.length ;i++)
	{
		if(ComparatorPerson.compare(input[i],biggest,new ComparatorString())>0)biggest=input[i];
	}
	
	return biggest;
		
	}
	
	@Override
	public int compareTo(Person o)
	{
		if(this == o){
			return 0;
		}
		
		if(o == null){
			return +1;
		}
		int result = compareObjects(birthDate, o.birthDate);
		if(result != 0){
			return result;
		}
		result = compareObjects(lastName,o.lastName);
		if(result != 0){return result;}
		result = compareObjects(firstName,o.firstName);
		if(result != 0){return result;}	
		return 0;
	}
	
	private int compareObjects(Comparable name1, Comparable name2) {
		if(name1 == name2)return 0;
		if(name1 == null)return -1;
		if(name2 == null)return +1;
		return name1.compareTo(name2);
	}

}

/**
 * Compares birth date first then last name and then first name.
 * If parameters are the same, method compares next.
 * 
 * @author Filip Kanak
 * 
 * @return returns 0 if all parameters are the same, returns 1 if Person2 is bigger, -1 if Person1 is bigger
 *
 */
class ComparatorPerson
{

	public static int compare(Object o1, Object o2, ComparatorString comparatorstring) {
		
		Date birthD1 = ((Person) o1).getBirthDate();
		Date birthD2 = ((Person) o2).getBirthDate();
		String lName1 = ((Person) o1).getLastName();
		String lName2 = ((Person) o2).getLastName();
		String fName1 = ((Person) o1).getFirstName();
		String fName2 = ((Person) o2).getFirstName();
		
		if(birthD1.compareTo(birthD2) > 0) 
		{
			return 1;
		}
		else
		{
			if(birthD1.compareTo(birthD2) < 0)
				{
					return -1;
				}
			else
			{
				if(comparatorstring.compare(lName1,lName2) > 0)
				{
					return 1;
				}
				else
				{
					if(comparatorstring.compare(lName1,lName2) < 0)
					{
						return -1;
					}
					else
					{
						if(comparatorstring.compare(fName1, fName2) > 0)
						{
							return 1;
						}
						else
						{
							if(comparatorstring.compare(fName1, fName2) < 0)
							{
								return -1;
							}
							else
							{
								return 0;
							}
								
						}
					}
				}
			}
		}
	}	
	
	
}

class ComparatorString implements Comparator<String>{
	public int compare(String a,String b){
		if(a == b){
			return 0;
		}
		if(a == null)
		{
		return -1;	
		}
		if(b == null)
		{
		 return +1;	
		}
		return a.compareTo(b);
	}
}	

class PersonComparator implements Comparator<Person>
{
	@Override
	public int compare(Person a,Person b){
		if(a == b){
			return 0;
		}
		if(a == null)
		{
		return -1;	
		}
		if(b == null)
		{
		 return +1;	
		}
		return a.compareTo(b);
	}
}