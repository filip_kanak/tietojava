import static org.junit.Assert.*;

import java.util.Comparator;

import org.junit.Before;
import org.junit.Test;

import com.tieto.training.lesson3.ArrayManipulation;

public class ArrayManipulationStringTest {
	
	private ArrayManipulation man;


@Before
public void setUp(){
	
	man = new ArrayManipulation();
	
}


@Test
public void testFindBiggestElementCommonScenario() {
	final String[] input = {"Anna","Filip","Vladim�r"};
	final String actual = man.findBiggestElement(input,new ComparatorString()); 
	assertEquals("Vladim�r",actual);
}

@Test
public void testFindBiggestElementWhichIsTheLast() {
	final String[] input = {"Anna","Filip","Vladim�r"};
	final String actual = man.findBiggestElement(input,new ComparatorString()); 
	assertEquals("Vladim�r",actual);
}

@Test
public void testFindBiggestElementWhichIsTheFirst() {
	final String[] input = {"Vladim�r","Anna","Filip"};
	final String actual = man.findBiggestElement(input,new ComparatorString()); 
	assertEquals("Vladim�r",actual);
}

@Test
public void testFindBiggestElementAllNegative() {
	final String[] input = {"","",""};
	final String actual = man.findBiggestElement(input,new ComparatorString()); 
	assertEquals("",actual);
}

@Test
public void testFindBiggestElementAllTheSame() {
	final String[] input = {"Vladim�r","Vladim�r","Vladim�r"};
	final String actual = man.findBiggestElement(input,new ComparatorString()); 
	assertEquals("Vladim�r",actual);
}

@Test
public void testFindBiggestElementEmptyArray() {
	final String[] input = {"","",""};
	final String actual = man.findBiggestElement(input,new ComparatorString()); 
	assertEquals("",actual);
}

@Test
public void testFindBiggestElementNull() {
	final String[] input = {null,null,null};
	final String actual = man.findBiggestElement(input,new ComparatorString()); 
	assertEquals(null,actual);
}}

class ComparatorString implements Comparator<String>{
	public int compare(String a,String b){
		if(a == b){
			return 0;
		}
		if(a == null)
		{
		return -1;	
		}
		if(b == null)
		{
		 return +1;	
		}
		return a.compareTo(b);
	}
	
	/*public int compare2(String a, String b)
	{
		if(a == b){
			return 0;
		}
		if(a == null)
		{
		return -1;	
		}
		if(b == null)
		{
		 return +1;	
		}
		for(int i=1;i<a.length();i++)
		{
			if(a.[i] > b[i]){}
		}
		return 0;
	}*/
	
}
