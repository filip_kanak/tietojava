package com.tieto.training.lesson2;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
	private static final double DELTA = 0.0001;
	private Calculator calc;

	@Before
	public void init() {
		calc = new Calculator();
	}

	@Test
	public void testAdd() {
		// prepare data
  		// call the method
		final double actual = calc.add(5.0, 3.0);

		// verify the result
		assertEquals(8.0, actual, DELTA);
	}

	@Test
	public void testHypotenuseCommon() {
		final double actual = calc.hypotenuse(3.0, 4.0);

		assertEquals(5.0, actual, DELTA);
	}
	
	@Test
	public void testHypotenuseNegative()
	{
		final double actual = calc.hypotenuse(-3.0, -14.0);
		assertEquals(14.31782106327635, actual, DELTA);
	}
}
