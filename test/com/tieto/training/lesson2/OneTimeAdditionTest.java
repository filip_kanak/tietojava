package com.tieto.training.lesson2;

import static org.junit.Assert.*;

import org.junit.Test;

public class OneTimeAdditionTest {

	

	@Test
	public void addNormalScenario() {
		OneTimeAddition oaa1 = new OneTimeAddition(10);
		OneTimeAddition oaa2 = new OneTimeAddition(20);
		
		final int actual1 = oaa1.add(5);
		final int actual2 = oaa2.add(5);
		
		assertEquals(15,actual1);
		assertEquals(25,actual2);
		
	}

	@Test(expected = IllegalStateException.class)
	public void addCannotBeCalledTwice()
	{
		final OneTimeAddition oaa = new OneTimeAddition(10);
		
		final int actual1 = oaa.add(5);
		
		assertEquals(15, actual1);
		
		oaa.add(2);
	}
	

}
