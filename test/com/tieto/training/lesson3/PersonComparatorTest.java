package com.tieto.training.lesson3;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class PersonComparatorTest {

	

	@Before
	public void setUp() throws Exception {
	
	}


	@Test
	public void testSmallerDate() {
		Person p1 = new Person("Karel","Nov�k",new Date(123456789));
		Person p2 = new Person("Jan","Petr",new Date(234567891));
		assertEquals(-1,ComparatorPerson.compare(p1,p2,new ComparatorString()));
	}
	
	@Test
	public void testBiggerDate() {
		Person p1 = new Person("Karel","Nov�k",new Date(234567891));
		Person p2 = new Person("Jan","Petr",new Date(123456789));
		assertEquals(1,ComparatorPerson.compare(p1,p2,new ComparatorString()));
	}
	
	@Test
	public void testSmallerLastName() {
		Person p1 = new Person("Karel","Nov�k",new Date(123456789));
		Person p2 = new Person("Jan","Petr",new Date(123456789));
		assertEquals(-1,ComparatorPerson.compare(p1,p2,new ComparatorString()));
	}
	
	@Test
	public void testBiggerLastName() {
		Person p1 = new Person("Karel","Petr",new Date(123456789));
		Person p2 = new Person("Jan","Nov�k",new Date(123456789));
		assertEquals(1,ComparatorPerson.compare(p1,p2,new ComparatorString()));
	}
	
	
	@Test
	public void testSmallerFirstName() {
		Person p1 = new Person("Jan","Nov�k",new Date(123456789));
		Person p2 = new Person("Karel","Nov�k",new Date(123456789));
		assertEquals(-1,ComparatorPerson.compare(p1,p2,new ComparatorString()));
	}
	
	@Test
	public void testBiggerFirstName() {
		Person p1 = new Person("Karel","Nov�k",new Date(123456789));
		Person p2 = new Person("Jan","Nov�k",new Date(123456789));
		assertEquals(1,ComparatorPerson.compare(p1,p2,new ComparatorString()));
	}
	
	@Test
	public void testEquals() {
		Person p1 = new Person("Jan","Nov�k",new Date(123456789));
		Person p2 = new Person("Jan","Nov�k",new Date(123456789));
		assertEquals(0,ComparatorPerson.compare(p1,p2,new ComparatorString()));
	}
	
	/*@Test
	
	public void testBiggestElement()
	{
		Person[] p = {new Person("Jan","Nov�k",new Date(123456789)),
						new Person("Karel","Nov�k",new Date(123456789))};
		assertSame(p[1],Person.findBiggestElement(p,new ComparatorPerson()));
	
	}*/
	
	

}
