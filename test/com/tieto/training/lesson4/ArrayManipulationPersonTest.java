package com.tieto.training.lesson4;


import static org.junit.Assert.*;

import java.util.Comparator;

import org.junit.Before;
import org.junit.Test;

import com.tieto.training.lesson3.ArrayManipulation;
import com.tieto.training.lesson3.Person;

public class ArrayManipulationPersonTest {
	
	private ArrayManipulation man;
	private Comparator<Person> comparator;
	
	@Before
	public void setUp(){
		
		man = new ArrayManipulation();
		
	}


	@Test
	public void testFindBiggestElementCommonScenario() {
		final int[] input = {-40,50,10,-9};
		final int actual = man.findBiggestElement(input); 
		assertEquals(50,actual);
	}
	
	@Test
	public void testFindBiggestElementWhichIsTheLast() {
		final int[] input = {-40,50,10,-9,100};
		final int actual = man.findBiggestElement(input);
		
		assertEquals(100,actual);
	}
	
	@Test
	public void testFindBiggestElementWhichIsTheFirst() {
		final int[] input = {101,-40,50,10,-9,100};
		final int actual = man.findBiggestElement(input); 
		assertEquals(101,actual);
	}
	
	@Test
	public void testFindBiggestElementAllNegative() {
		final int[] input = {-40,-50,-23,-10,-9};
		final int actual = man.findBiggestElement(input); 
		assertEquals(-9,actual);
	}
	
	@Test
	public void testFindBiggestElementAllTheSame() {
		final int[] input = {-40,-40,-40,-40,-40};
		final int actual = man.findBiggestElement(input); 
		assertEquals(-40,actual);
	}
	
	@Test
	public void testFindBiggestElementEmptyArray() {
		final int[] input = {};
		final int actual = man.findBiggestElement(input); 
		assertEquals(Integer.MIN_VALUE,actual);
	}

	@Test
	public void testFindBiggestElementNull() {
		final int[] input = null;
		final int actual = man.findBiggestElement(input); 
		assertEquals(Integer.MIN_VALUE,actual);
	}
}


