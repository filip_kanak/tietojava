package com.tieto.training.lesson4;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;

import com.tieto.training.arraylist.MyArrayList;

public abstract class ListTest {

	protected MyArrayList<String> list;
	protected MyArrayList<String> list2;
	protected MyArrayList<Integer> listi;
	
	protected static final String S01 = "David";
	protected static final String S02 = "Filip";
	protected static final String S03 = "Daniel";
	protected static final String S04 = "Simona";
	protected static final String S05 = "Jakub";
	protected static final String S06 = "Vladim�r";
	protected static final String S07 = "Denis";
	protected static final String S08 = "V�clav";
	protected static final String S09_02 = "Filip";
	protected static final String S10_05 = "Jakub";
	protected static final String S11 = "Kry�tof";
	protected static final String S12_02 = "Filip";
	protected static final String S13 = "Jan";
	
	

	@Before
	public void setUp() throws Exception {
		list = new MyArrayList<>();
		list2 = new MyArrayList<>();
		listi = new MyArrayList<>();
	}


	}


