package com.tieto.training.lesson4;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import com.tieto.training.lesson3.Person;

public class PersonTest {

	

	@Test
	public void testEquals() {
		final Date birthDate = new Date();
		final Person p1 = new Person(new String("Karel"), "Nov�k", birthDate);
		final Person p2 = new Person("Karel", "Nov�k", birthDate);
		
		assertNotSame(p1,p2);
		assertEquals(p1,p2);
	}
	
	@Test
	public void compareDifferentDates(){
		final Date date1 = new Date(92,0,1);
		final Date date2 = new Date(92,0,2);
		final Person p1 = new Person("Adam","Adamus",date1);
		final Person p2 = new Person("Bo�ena","�of�kov�",date2);
		int actual = p1.compareTo(p2);
		assertTrue(actual < 0);		
	}
	
	@Test
	public void compareSameDatesDifferentLastName(){
		final Date date1 = new Date(92,0,1);
		final Person p1 = new Person("Adam","Adamus",date1);
		final Person p2 = new Person("Bo�ena","�of�kov�",date1);
		int actual = p1.compareTo(p2);
		assertTrue(actual < 0);		
	}
	
	@Test
	public void compareSameDatesDifferentFirstName(){
		final Date date1 = new Date(92,0,1);
		final Person p1 = new Person("Adam","Adamus",date1);
		final Person p2 = new Person("Bo�ena","Adamus",date1);
		int actual = p1.compareTo(p2);
		assertTrue(actual < 0);		
	}
	
	@Test
	public void compareSame(){
		final Date date1 = new Date(92,0,1);
		final Person p1 = new Person("Adam","Adamus",date1);
		final Person p2 = new Person("Adam","Adamus",date1);
		int actual = p1.compareTo(p2);
		assertTrue(actual == 0);		
	}
	
	@Test
	public void compareFirstDateNotExists(){
		final Date date1 = new Date(92,0,1);
		final Person p1 = new Person("Adam","Adamus",null);
		final Person p2 = new Person("Bo�ena","�of�kov�",date1);
		int actual = p1.compareTo(p2);
		assertTrue(actual < 0);		
	}
	
	@Test
	public void compareSecondDateNotExists(){
		final Date date1 = new Date(92,0,1);
		final Person p1 = new Person("Adam","Adamus",date1);
		final Person p2 = new Person("Bo�ena","�of�kov�",null);
		int actual = p1.compareTo(p2);
		assertTrue(actual > 0);		
	}
	
	@Test
	public void compareDatesNotExists(){
		final Date date1 = new Date(92,0,1);
		final Person p1 = new Person("Adam","Adamus",null);
		final Person p2 = new Person("Bo�ena","�of�kov�",null);
		int actual = p1.compareTo(p2);
		assertTrue(actual < 0);		
	}
	
	@Test
	public void compareSameDatesFirstLastNameNotExists(){
		final Date date1 = new Date(92,0,1);
		final Person p1 = new Person("Adam",null,date1);
		final Person p2 = new Person("Bo�ena","�of�kov�",date1);
		int actual = p1.compareTo(p2);
		assertTrue(actual < 0);		
	}
	
	@Test
	public void compareSameDatesSecondLastNameNotExists(){
		final Date date1 = new Date(92,0,1);
		final Person p1 = new Person("Adam","Adamus",date1);
		final Person p2 = new Person("Bo�ena",null,date1);
		int actual = p1.compareTo(p2);
		assertTrue(actual > 0);		
	}
	
	@Test
	public void compareSameDatesLastNamesNotExists(){
		final Date date1 = new Date(92,0,1);
		final Person p1 = new Person("Adam",null,date1);
		final Person p2 = new Person("Bo�ena",null,date1);
		int actual = p1.compareTo(p2);
		assertTrue(actual < 0);		
	}
	
	@Test
	public void compareSameDatesFirstFirstNameNotExists(){
		final Date date1 = new Date(92,0,1);
		final Person p1 = new Person(null,"Adamus",date1);
		final Person p2 = new Person("Bo�ena","Adamus",date1);
		int actual = p1.compareTo(p2);
		assertTrue(actual < 0);		
	}
	
	@Test
	public void compareSameDatesSecondFirstNameNotExists(){
		final Date date1 = new Date(92,0,1);
		final Person p1 = new Person("Adam","Adamus",date1);
		final Person p2 = new Person(null,"Adamus",date1);
		int actual = p1.compareTo(p2);
		assertTrue(actual > 0);		
	}
	
	@Test
	public void compareSameDatesFirstNamesNotExists(){
		final Date date1 = new Date(92,0,1);
		final Person p1 = new Person(null,"Adamus",date1);
		final Person p2 = new Person(null,"Adamus",date1);
		int actual = p1.compareTo(p2);
		assertTrue(actual == 0);		
	}
	
	@Test
	public void compareSecondPersonIsNull(){
		final Date date1 = new Date(92,0,1);
		final Person p1 = new Person("Bo�ena","�of�kov�",date1);
		final Person p2 = null;
		int actual = p1.compareTo(p2);
		assertTrue(actual > 0);		
	}
}
