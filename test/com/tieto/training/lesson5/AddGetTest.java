package com.tieto.training.lesson5;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.tieto.training.lesson4.ListTest;

public class AddGetTest extends ListTest {
	@Test
	public void basicAddGet() {
		assertEquals(0, list.size());
		assertTrue(list.isEmpty());
		
		list.add("Hello");
		
		assertEquals(1, list.size());
		assertFalse(list.isEmpty());
		
		list.add("Nazdar");
		
		assertEquals(2, list.size());
		assertFalse(list.isEmpty());
	}

	@Test
	public void bulkAddandClear(){
		
		final int BIG_LIST_SIZE = 10_000;
		for(int i=0;i<BIG_LIST_SIZE;i++)
		{
			list.add(String.valueOf(i));
		}
		assertEquals(BIG_LIST_SIZE,list.size());
		list.clear();
		
		assertEquals(0, list.size());
		assertTrue(list.isEmpty());
	}
	
	@Test
	public void clearEmptyList()
	{
		assertEquals(0, list.size());
		assertTrue(list.isEmpty());
		
		list.clear();
		
		assertEquals(0, list.size());
		assertTrue(list.isEmpty());
		
		list.clear();
		
		assertEquals(0, list.size());
		assertTrue(list.isEmpty());
	}
	
	@Test
	public void addGet()
	{
		final String item0 = "Hello";
		final String item1 = "Nazdar";
		assertEquals(0, list.size());
		assertTrue(list.isEmpty());
		
		list.add(item0);
		
		assertEquals(1, list.size());
		assertFalse(list.isEmpty());
		
		list.add(item1);
		
		assertEquals(2, list.size());
		assertFalse(list.isEmpty());
		
		assertSame(item0,list.get(0));
		assertSame(item1,list.get(1));
	}
	
	@Test
	public void contains()
	{
		final String item0 = "Hello";
		final String item1 = "Nazdar";
		assertEquals(0, list.size());
		assertTrue(list.isEmpty());
		
		list.add(item0);
		
		assertEquals(1, list.size());
		assertFalse(list.isEmpty());
		
		list.add(item1);
		
		assertEquals(2, list.size());
		assertFalse(list.isEmpty());
		assertTrue(list.contains("Hello"));
	}
	//TODO Implement equals in MyArrayList
	@Test
	public void equals()
	{
		
		final String item0 = "Hello";
		final String item1 = "Nazdar";
		
		list.add(item0);
		list.add(item1);
		
		list2.add(item0);
		list2.add(item1);
		
		assertTrue(list.equals(list2));	
	}
	
	@Test
	public void indexOfAndLastIndexOf()
	{
		final String item0 = "Hello";
		final String item1 = "Nazdar";
		final String item2 = "Hello";
		
		list.add(item0);
		list.add(item1);
		list.add(item2);
		
		assertEquals(0,list.indexOf(item0));
		assertEquals(2,list.lastIndexOf(item0));
	}
	
	@Test
	public void remove()
	{
		final String item0 = "Hello";
		final String item1 = "Nazdar";
		
		list.add(item0);
		list.add(item1);
		
		assertEquals(item0,list.remove(0));
		assertTrue(list.remove(item1));
	}
	
	@Test
	public void set()
	{
		final String item0 = "Hello";
		final String item1 = "Nazdar";
		
		list.add(item0);
		
		assertEquals("Hello",list.set(0, item1));
	}
	@Test
	public void toArray()
	{
		final String item0 = "Hello";
		final String[] p = {"Hello"};
		
		list.add(item0);
		
		assertArrayEquals(p,list.toArray());
	}
	//TODO Implement work with Collections
	/*@Test
	public void addAll()
	{
		final String item0 = "Hello";
		final String item1 = "Nazdar";
		
		list.add(item0);
		list.add(item1);
		
		list2.add(item0);
		list2.add(item1);
		
		
		assertTrue(list.addAll(list2));
		assertEquals("Nazdar",list.get(3));
	}*/
	//TODO Implement work with Collections
	/*@Test
	public void containsAll()
	{
		
		final String item0 = "Hello";
		final String item1 = "Nazdar";
		
		list.add(item0);
		list.add(item1);
		
		list2.add(item0);
		list2.add(item1);
		
		assertTrue(list.containsAll(list2));
		
	}*/
	//TODO Implement work with Collections
	/*@Test
	public void removeAll()
	{
		final String item0 = "Hello";
		final String item1 = "Nazdar";
		final String item2 = "Bye";
		
		list.add(item0);
		list.add(item1);
		list.add(item2);
		
		list2.add(item0);
		list2.add(item1);
		
		assertTrue(list.removeAll(list2));
		assertEquals("Bye",list.get(0));
	}*/
	
	/*@Test
	public void replaceAll()
	{
		final Integer item0 = 1;
		final Integer item1 = 2;
		final Integer item2 = 3;
		
		listi.add(item0);
		listi.add(item1);
		listi.add(item2);
		
		list.replaceAll();
	}*/

	@Test
	public void addDuplicateValues()
	{
		list.add( S01);
		list.add( S02);
		list.add( S03) ;
		list.add( S04);
		list.add( S05) ;
		list.add( S06) ;
		list.add( S07);
		list.add( S08);
		list.add( S09_02) ;
		list.add( S10_05) ;
		list.add( S11) ;
		list.add( S12_02) ;
		list.add( S13) ;
		
		assertEquals(13,list.size());
		assertSame(S03,list.get(2));
		assertSame(S09_02,list.get(8));
	}
	
	@Test
	public void addAcceptNullValues()
	{
		list.add( S01);
		list.add(null);
		list.add( S03) ;
		list.add(null);
		
		assertEquals(4, list.size());
		//assertEquals(null, list.get(0));
		assertEquals(null, list.get(1));
		//assertEquals(null, list.get(2));
		assertEquals(null, list.get(3));
		
	}
	
}



