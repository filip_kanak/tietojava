package com.tieto.training.lesson5;

import static org.junit.Assert.*;

import org.junit.Test;

import com.tieto.training.lesson4.ListTest;

public class RemoveTest extends ListTest{

	
	
	@Test
	public void basicRemove() 
	{
		//preparation data
		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);
		
		assertEquals(4,list.size());
		
		//call the method
		list.remove(1);
		
		//verify the result
		
		assertEquals(3,list.size());
		assertSame(S01, list.get(0));
		assertSame(S03, list.get(1));
		assertSame(S04, list.get(2));
		
	}
	
	@Test
	public void multipleRemove()
	{
		list.add( S01);
		list.add( S02);
		list.add( S03) ;
		list.add( S04);
		list.add( S05) ;
		list.add( S06) ;
		list.add( S07);
		list.add( S08);
		list.add( S09_02) ;
		list.add( S10_05) ;
		list.add( S11) ;
		list.add( S12_02) ;
		list.add( S13) ;
		
		assertEquals(13,list.size());
		
		list.remove(3);
		list.remove(7);
		list.remove(7);
		
		assertEquals(10,list.size());
		
		assertSame( S01, list.get(0));
		assertSame( S02, list.get(1));
		assertSame( S03, list.get(2)) ;		
		assertSame( S05, list.get(3)) ;
		assertSame( S06, list.get(4)) ;
		assertSame( S07, list.get(5));
		assertSame( S08, list.get(6));	
		
		assertSame( S11, list.get(7)) ;
		assertSame( S12_02, list.get(8)) ;
		assertSame( S13, list.get(9)) ;
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void removeBeforeSize()
	{
		
		removeOnIndex(-1);
		
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void removeJustBehindSize()
	{
		
		removeOnIndex(3);
		
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void removeBehindSize()
	{
		
		removeOnIndex(100);
		
	}
	
	private void removeOnIndex(int index)
	{
		list.add(S01);
		list.add(S02);
		list.add(S03);

		list.remove(index);
		
	}
	
	@Test
	public void removeObject()
	{
		list.add( S01);
		list.add( S02);
		list.add( S03) ;
		list.add( S04);
		list.add( S05) ;
		list.add( S06) ;
		list.add( S07);
		list.add( S08);
		list.add( S09_02) ;
		list.add( S10_05) ;
		list.add( S11) ;
		list.add( S12_02) ;
		list.add( S13) ;
		
		assertTrue(list.remove(S02));
		assertEquals(S03,list.get(1));
	}
	
	@Test
	public void removeMultipleObjests()
	{
		list.add( S01);
		list.add( S02);
		list.add( S03) ;
		list.add( S04);
		list.add( S05) ;
		list.add( S06) ;
		list.add( S07);
		list.add( S08);
		list.add( S09_02) ;
		list.add( S10_05) ;
		list.add( S11) ;
		list.add( S12_02) ;
		list.add( S13) ;
		
		while(list.remove(S02));
		assertEquals(10,list.size());
		assertEquals(S13,list.get(9));
	}
	
	@Test
	public void removeObjectNotExists()
	{
		list.add( S01);
		list.add( S02);
		list.add( S03) ;
		list.add( S04);
		list.add( S05) ;
		list.add( S06) ;
		list.add( S07);
		list.add( S08);
		list.add( S09_02) ;
		list.add( S10_05) ;
		list.add( S11) ;
		list.add( S12_02) ;
		list.add( S13) ;
		
		assertFalse(list.remove("Hoj"));
	}
}
