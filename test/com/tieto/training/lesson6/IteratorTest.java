package com.tieto.training.lesson6;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import org.junit.Test;

import com.tieto.training.lesson4.ListTest;

public class IteratorTest extends ListTest {

	@Test
	public void iterateOverList() {
		//ArrayList<String> list = new ArrayList<String>();
		
		final String item0 = "Hello";
		final String item1 = "Nazdar";
		final String item2 = "Bye";
		
		list.add(item0);
		list.add(item1);
		list.add(item2);
		
		Iterator<String> iterator = list.iterator();
		
		int index=0;
		while (iterator.hasNext()) {
			assertSame(list.get(index),iterator.next());
			index++;
		}
	}
	
	@Test
	public void iterateEmptyList()
	{
	//	ArrayList<String> list = new ArrayList<String>();
		
		Iterator<String> iterator = list.iterator();
		
		int index=0;
		while (iterator.hasNext()) {
			assertSame(list.get(index),iterator.next());
			index++;
		}
	}
	
	/*@Test
	public void iterateAfterEnd(){
		Iterator<String> iterator = list.iterator();
		
	}*/

	@Test
	public void iterateOverListAndBack() {
		ArrayList<String> list = new ArrayList<String>();
		
		final String item0 = "Hello";
		final String item1 = "Nazdar";
		final String item2 = "Bye";
		
		list.add(item0);
		list.add(item1);
		list.add(item2);
		
		ListIterator<String> iterator = list.listIterator();
		assertFalse(iterator.hasPrevious());
		
		int index=0;
		while (iterator.hasNext()) {
			assertEquals(index-1,iterator.previousIndex());
			assertEquals(index,iterator.nextIndex());
			assertSame(list.get(index),iterator.next());
			assertEquals(index,iterator.previousIndex());
			assertEquals(index+1,iterator.nextIndex());
			
			index++;
		}
		
		assertFalse(iterator.hasNext());
		
		while (iterator.hasPrevious()) {
			index--;
			
			assertEquals(index,iterator.previousIndex());
			assertEquals(index+1,iterator.nextIndex());
			assertSame(list.get(index),iterator.previous());
			assertEquals(index-1,iterator.previousIndex());
			assertEquals(index,iterator.nextIndex());
		}
		
		assertFalse(iterator.hasPrevious());
	}
	
	@Test
	public void iteratorAdd()
	{
		
		ListIterator<String> iterator = list.listIterator();
		final String item0 = "Hello";
		final String item1 = "Nazdar";
		final String item2 = "Bye";
		
		list.add(item0);
		list.add(item1);
		list.add(item2);
		iterator.next();
		
		iterator.add(S01);
		
		assertEquals(4,list.size());
		assertEquals(1,iterator.previousIndex());
		assertEquals(2,iterator.nextIndex());
		assertSame(S01,iterator.previous());
		assertSame(S01,iterator.next());
		assertSame(item1,iterator.next());
	}
	
	@Test
	public void iteratorRemove()
	{
		ListIterator<String> iterator = list.listIterator();
		final String item0 = "Hello";
		final String item1 = "Nazdar";
		final String item2 = "Bye";
		
		list.add(item0);
		list.add(item1);
		list.add(item2);
		
		assertSame(item0,iterator.next());
		iterator.add(S01);
		assertSame(S01,iterator.previous());
		
		assertEquals(4,list.size());
		
		iterator.remove();
		
		assertEquals(3,list.size());
		assertEquals(1,iterator.nextIndex());
		assertSame(item0,iterator.previous());
		assertSame(item0,iterator.next());
		assertSame(item1,iterator.next());	
		assertSame(item2,iterator.next());
	}
	
	@Test
	public void set()
	{	ListIterator<String> iterator = list.listIterator();
		iterator.add(S01);
		assertSame(S01,iterator.previous());
		iterator.set(S02);
		assertSame(S02,iterator.next());
	}
}
