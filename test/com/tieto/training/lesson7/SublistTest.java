package com.tieto.training.lesson7;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tieto.training.arraylist.MyArrayList;
import com.tieto.training.lesson4.ListTest;

public class SublistTest extends ListTest{
	MyArrayList<String> list;
	@Before
	public void setUp() throws Exception {
		list = new MyArrayList<String>();
		list.add( S01);
		list.add( S02);
		list.add( S03) ;
		list.add( S04);
		list.add( S05) ;
		list.add( S06) ;
		list.add( S07);
		list.add( S08);
		list.add( S09_02) ;
		list.add( S10_05) ;
		list.add( S11) ;
		list.add( S12_02) ;
		list.add( S13) ;
	}

	@Test
	public void sublistBasicScenario() {
		final List<String> sublist = list.subList(5,10);
		assertSame(S01,list.get(0));
		assertEquals(5,sublist.size());
		assertEquals(S06,sublist.get(0));
		assertEquals(S10_05,sublist.get(4));
	}
	
	@Test
	public void sublistRemove()
	{
		final List<String> sublist = list.subList(5,10);
		
		sublist.remove(2);
		
		assertEquals(4,sublist.size());
		
		assertSame(list.get(7),sublist.get(2));
		
	}
	
	
	@Test(expected = RuntimeException.class)
	public void sublistIncorrectBounds()
	{
		@SuppressWarnings("unused")
		final List<String> sublist = list.subList(11,10);
		
	}
	
	@Test
	public void sublistSet()
	{
		final List<String> sublist = list.subList(5,10);
		
		sublist.set(0, S11);
		
		assertSame(S11,sublist.get(0));
		assertSame(S11,list.get(5));
	}
	
	@Test
	public void sublistClear()
	{
		final List<String> sublist = list.subList(5,10);
		
		sublist.clear();
		
		assertEquals(0,sublist.size());
		assertSame(S11,list.get(5));
	}
	
	@Test
	public void sublistAdd()
	{
		/*List<String> list = new ArrayList<String>();
		list.add( S01);
		list.add( S02);
		list.add( S03) ;
		list.add( S04);
		list.add( S05) ;
		list.add( S06) ;
		list.add( S07);
		list.add( S08);
		list.add( S09_02) ;
		list.add( S10_05) ;
		list.add( S11) ;
		list.add( S12_02) ;
		list.add( S13) ;*/
		final List<String> sublist = list.subList(5,10);
		sublist.add(S01);
		
		assertSame(S01,list.get(10));
	}

}
