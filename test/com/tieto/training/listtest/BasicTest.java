package com.tieto.training.listtest;


import static org.junit.Assert.assertEquals;


import org.junit.Test;

public class BasicTest extends ListTest {




	@Test
	public void AddDuplicateValuesTest() {
		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);
		list.add(S05);
		list.add(S06);
		list.add(S07);
		list.add(S08);
		list.add(S09_02);
		list.add(S10_07);
		list.add(S11);
		assertEquals(11, list.size());
		assertEquals(S02, list.get(1));
		assertEquals(S07, list.get(6));

	}

	@Test
	public void NullValuesTest() {
		list.add(S01);
		list.add(null);
		list.add(S03);
		assertEquals(null, list.get(1));
		assertEquals(S03, list.get(2));

	}

}
