package com.tieto.training.listtest;




import java.util.List;

import org.junit.Before;

import com.tieto.training.arraylist.MyArrayList;
public abstract class ListTest {
	
	protected final static String S01="David";
	protected final static String S02="Filip";
	protected final static String S03="Roman";
	protected final static String S04="Zikmund";
	protected final static String S05="Roman";
	protected final static String S06="Ondra";
	protected final static String S07="Jakub";
	protected final static String S08="Petr";
	protected final static String S09_02="Filip";
	protected final static String S10_07="Jakub";
	protected final static String S11="Honza";

	
	protected List<String> list;

	@Before
	public void setUp() throws Exception {
	list = new MyArrayList<>();
	}

}