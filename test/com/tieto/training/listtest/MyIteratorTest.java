package com.tieto.training.listtest;


import static org.junit.Assert.*;

import java.util.ListIterator;
import org.junit.Test;
import java.util.NoSuchElementException;

public class MyIteratorTest extends ListTest {

	@Test
	public void myArrayMyListIteratorHasNextNextIndexPreviousIndexAKABasicTest() {
		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);

		final ListIterator<String> iter = list.listIterator();
		assertTrue(iter.hasNext());
		assertFalse(iter.hasPrevious());

		assertEquals(-1, iter.previousIndex());

		iter.next();
		iter.next();

		assertTrue(iter.hasNext());
		assertTrue(iter.hasPrevious());

		assertEquals(1, iter.previousIndex());
		assertEquals(2, iter.nextIndex());

		iter.next();
		iter.next();

		assertFalse(iter.hasNext());

	}

	@Test(expected = NoSuchElementException.class)
	public void myArrayMyListIteratorNextTest() {
		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);

		final ListIterator<String> iter = list.listIterator();
		iter.next();
		iter.next();
		iter.next();
		iter.next();
		iter.next();

	}

	@Test
	public void myArrayMyListIteratorRemoveTest() {
		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);

		final ListIterator<String> iter = list.listIterator();
		iter.next();
		iter.remove();
		iter.previous();
		iter.remove();
	}
	@Test(expected = IllegalStateException.class)
	public void myArrayMyListIteratorRemoveErrorTest() {
		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);
		final ListIterator<String> iter = list.listIterator();
		iter.remove();
		iter.next();

	}
	@Test
	public void myArrayMyListIteratorSetTest() {
		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);

		final ListIterator<String> iter = list.listIterator();
		iter.next();
		iter.set(S06);
		iter.previous();
		iter.set(S01);
	}
	@Test(expected = IllegalStateException.class)
	public void myArrayMyListIteratorSetErrorTest() {
		final ListIterator<String> iter = list.listIterator();
		iter.set(S06);
		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);

	}
	@Test
	public void myArrayMyListIteratorAddTest() {
		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);

		final ListIterator<String> iter = list.listIterator();
		iter.next();
		iter.add(S04);

		assertEquals(5, list.size());
	}
}
