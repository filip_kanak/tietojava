package com.tieto.training.listtest;


import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.junit.Test;

public class MyListIteratorTest extends ListTest {

	@Test
	public void iteratorBasicTest() {
		List<String> list = new ArrayList<>();

		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);
		list.add(S05);
		list.add(S06);
		list.add(S07);
		list.add(S08);

		final Iterator<String> iter = list.iterator();
		int index = 0;
		while (iter.hasNext()) {
			final String element = iter.next();
			assertSame(list.get(index), element);
			index++;
		}

	}

	@Test
	public void ListIteratorBasicTest() {
		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);
		list.add(S05);
		list.add(S06);
		list.add(S07);
		list.add(S08);

		final Iterator<String> iter = list.iterator();
		int index = 0;
		while (iter.hasNext()) {
			final String element = iter.next();
			assertSame(list.get(index), element);
			index++;
		}

	}

	@Test
	public void AdvancedListIterator() {
		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);
		list.add(S05);

		list = new ArrayList<>();
		final ListIterator<String> iter = list.listIterator();

		int index = 0;
		while (iter.hasNext()) {
			final String element = iter.next();
			assertSame(index, iter.nextIndex());
			assertSame(index - 1, iter.previous());
			assertEquals(index, iter.nextIndex());
			assertEquals(index - 1, iter.previousIndex());
			assertEquals(element,iter.previous());
			index++;
		}

		while (iter.hasNext()) {
			final String element = iter.next();
			assertSame(index, iter.nextIndex());
			assertSame(list.get(index), element);
			assertSame(index - 1, iter.previous());
			assertEquals(element,iter.previous());
			index--;
		}

	}
	@Test
	public void listIteratorTest() {
		List<String> list = new ArrayList<>();
		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);
		
		final ListIterator<String> iter = list.listIterator();
		
		iter.next();
		iter.next();
		assertEquals(1, iter.previousIndex());
		assertEquals(2, iter.nextIndex());
		assertEquals(4, list.size());
		
		iter.next();
		assertEquals(2, iter.previousIndex());
		assertEquals(3, iter.nextIndex());
		assertEquals(4, list.size());
		
		iter.add(S07);
		assertEquals(5, list.size());
		assertEquals(3, iter.previousIndex());
		assertEquals(4, iter.nextIndex());
		

	}
	@Test
	public void myArrayListIteratorTest() {
		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);
		
		final ListIterator<String> iter = list.listIterator();
		
		iter.next();
		iter.next();
		assertEquals(1, iter.previousIndex());
		assertEquals(2, iter.nextIndex());
		assertEquals(4, list.size());
		
		iter.next();
		assertEquals(2, iter.previousIndex());
		assertEquals(3, iter.nextIndex());
		assertEquals(4, list.size());
		
		iter.add(S07);
		assertEquals(5, list.size());
		assertEquals(3, iter.previousIndex());
		assertEquals(4, iter.nextIndex());
		

	}

}
