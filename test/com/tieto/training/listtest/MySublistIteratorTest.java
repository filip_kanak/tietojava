package com.tieto.training.listtest;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ListIterator;

import org.junit.Before;
import org.junit.Test;

import com.tieto.training.arraylist.MyArrayList;
import com.tieto.training.lesson4.ListTest;

public class MySublistIteratorTest extends ListTest {
	MyArrayList<String> list;

	@Before
	public void setUp() throws Exception {
		list = new MyArrayList<String>();
		list.add(S01);
		list.add(S02);
		list.add(S03);
		list.add(S04);
		list.add(S05);
		list.add(S06);
		list.add(S07);
		list.add(S08);
		list.add(S09_02);
		list.add(S10_05);
		list.add(S11);
		list.add(S12_02);
		list.add(S13);
	}

	@Test
	public void basictest() {
		final List<String> sublist = list.subList(5, 10);
		final ListIterator<String> iter = sublist.listIterator();

		assertTrue(iter.hasNext());
		assertFalse(iter.hasPrevious());

		assertEquals(-1, iter.previousIndex());
		assertEquals(0, iter.nextIndex());
	}

	@Test
	public void basicValuetest() {
		final List<String> sublist = list.subList(5, 10);
		final ListIterator<String> iter = sublist.listIterator();
		int index = 0;
		while (iter.hasNext()) {
			assertSame(sublist.get(index), iter.next());
			index++;
		}
	}

	@Test
	public void basicIndexest() {
		final List<String> sublist = list.subList(5, 10);
		final ListIterator<String> iter = sublist.listIterator();
		int index = 0;
		while (iter.hasNext()) {
			assertEquals(index - 1, iter.previousIndex());
			assertEquals(index, iter.nextIndex());
			assertSame(sublist.get(index), iter.next());
			assertEquals(index, iter.previousIndex());
			assertEquals(index + 1, iter.nextIndex());

			index++;
		}
	}

	@Test
	public void setTest() {
		final List<String> sublist = list.subList(5, 10);
		final ListIterator<String> iter = sublist.listIterator();
		
		iter.next();
		iter.set(S02);
		assertSame(S07, iter.next());
	}
	
	@Test
	public void removeTest() {
		final List<String> sublist = list.subList(5, 10);
		final ListIterator<String> iter = sublist.listIterator();
		assertEquals(5,sublist.size());
		iter.next();
		iter.remove();
		assertEquals(4,sublist.size());	}
}
