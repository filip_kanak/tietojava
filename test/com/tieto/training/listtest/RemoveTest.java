package com.tieto.training.listtest;



import static org.junit.Assert.*;

import org.junit.Test;

public class RemoveTest extends ListTest {

	@Test
	public void removeTest() {
		list.add(S01);
		list.add(S02);
		list.add(S04);
		list.add(S11);

		assertSame(4, list.size());

		list.remove(1);

		assertSame(3, list.size());

		assertSame(S01, list.get(0));
		assertSame(S04, list.get(1));
		assertSame(S11, list.get(2));
	}

	@Test
	public void MultiRemovetest() {
		list.add(S01); // 0
		list.add(S02); // 1
		list.add(S03); // 2
		list.add(S04); // 3
		list.add(S05); // 4
		list.add(S06); // 5
		list.add(S07); // 6
		list.add(S08); // 7
		list.add(S09_02); // 8
		list.add(S10_07); // 9
		list.add(S11); // 10

		assertSame(11, list.size());
		list.remove(5);
		list.remove(3);
		list.remove(3);
		assertSame(8, list.size());

		assertSame(S01, list.get(0));
		assertSame(S02, list.get(1));
		assertSame(S03, list.get(2));
		assertSame(S07, list.get(3));
		assertSame(S08, list.get(4));
		assertSame(S09_02, list.get(5));
		assertSame(S10_07, list.get(6));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void removeOverSizeTest() {
		removeOnIndexTest(5);
		}

	@Test(expected = IndexOutOfBoundsException.class)
	public void removeNegativeSizeTest() {
		removeOnIndexTest(-5);
		}

	public void removeOnIndexTest(int index) {
		list.add(S01);
		list.add(S02);
		list.add(S04);

		list.remove(index);

	}

}
