package com.tieto.training.listtest;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.junit.Test;

public class SubListTest extends ListTest {

	@Test
	public void basicTest() {
		List<String> list = new ArrayList<>();

		list.add(S01); // 0
		list.add(S02); // 1
		list.add(S03); // 2
		list.add(S04); // 3
		list.add(S05); // 4
		list.add(S06); // 5
		list.add(S07); // 6
		list.add(S08); // 7
		list.add(S09_02); // 8
		list.add(S10_07); // 9
		list.add(S11); // 10
		list.add(S02); // 11
		assertEquals(12, list.size());
		final List<String> sublist = list.subList(5, 10);

		assertEquals(5, sublist.size());
		assertEquals(S06, sublist.get(0));
		assertEquals(S07, sublist.get(1));
		assertEquals(S08, sublist.get(2));
		assertEquals(S09_02, sublist.get(3));
	}

	@Test
	public void subListClearTest() {
		List<String> list = new ArrayList<>();

		list.add(S01); // 0
		list.add(S02); // 1
		list.add(S03); // 2
		list.add(S04); // 3
		list.add(S05); // 4
		list.add(S06); // 5
		list.add(S07); // 6
		list.add(S08); // 7
		list.add(S09_02); // 8
		list.add(S10_07); // 9
		list.add(S11); // 10
		list.add(S02); // 11
		final List<String> sublist = list.subList(5, 10);
		sublist.clear();
		assertEquals(7, list.size());
		assertEquals(S11, list.get(5));
		assertEquals(S01, list.get(0));
		assertEquals(S02, list.get(6));

	}

	@Test
	public void subListIsEmptyTest() {
		List<String> list = new ArrayList<>();

		list.add(S01); // 0
		list.add(S02); // 1
		list.add(S03); // 2
		list.add(S04); // 3
		list.add(S05); // 4
		list.add(S06); // 5
		list.add(S07); // 6
		list.add(S08); // 7
		list.add(S09_02); // 8
		list.add(S10_07); // 9
		list.add(S11); // 10
		list.add(S02); // 11
		final List<String> sublist = list.subList(4, 6);
		assertFalse(sublist.isEmpty());
		sublist.clear();
		assertTrue(sublist.isEmpty());

	}

	@Test
	public void subListhcontainsTest() {
		List<String> list = new ArrayList<>();

		list.add(S01); // 0
		list.add(S02); // 1
		list.add(S03); // 2
		list.add(S04); // 3
		list.add(S05); // 4
		list.add(S06); // 5
		list.add(S07); // 6
		list.add(S08); // 7
		list.add(S09_02); // 8
		list.add(S10_07); // 9
		list.add(S11); // 10
		list.add(S02); // 11
		final List<String> sublist = list.subList(3, 6);
		assertFalse(sublist.contains(S02));
		assertTrue(sublist.contains(S04));
		assertFalse(sublist.contains(S07));
	}

	@Test
	public void subListIteratorTest() {
		List<String> list = new ArrayList<>();

		list.add(S01); // 0
		list.add(S02); // 1
		list.add(S03); // 2
		list.add(S04); // 3
		list.add(S05); // 4
		list.add(S06); // 5
		list.add(S07); // 6
		list.add(S08); // 7
		list.add(S09_02); // 8
		list.add(S10_07); // 9
		list.add(S11); // 10
		list.add(S02); // 11
		final List<String> sublist = list.subList(5, 10);
		final Iterator<String> iter = sublist.iterator();
		int index = 0;
		while (iter.hasNext()) {
			final String element = iter.next();
			assertSame(sublist.get(index), element);
			index++;
		}
	}

	@Test
	public void subListhRemoveTest() {
		List<String> list = new ArrayList<>();

		list.add(S01); // 0
		list.add(S02); // 1
		list.add(S03); // 2
		list.add(S04); // 3
		list.add(S05); // 4
		list.add(S06); // 5
		list.add(S07); // 6
		list.add(S08); // 7
		list.add(S09_02); // 8
		list.add(S10_07); // 9
		list.add(S11); // 10
		list.add(S02); // 11
		final List<String> sublist = list.subList(3, 7);
		assertEquals(4, sublist.size());
		sublist.remove(2);
		assertEquals(3, sublist.size());
	}

}
