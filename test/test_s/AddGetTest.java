package test_s;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class AddGetTest extends ListTest {

	@Test
	public void isEmptyTest() {
	assertTrue(list.isEmpty());

	list.add("Hello");
	assertEquals(1, list.size());
	assertFalse(list.isEmpty());

	}

	@Test
	public void containsTest() {
	String hello = "Hello";
	String hi = "Hi";
	list.add(hello);
	list.add("ahoj");
	String nullString = null;
	assertTrue(list.contains(hello));
	assertFalse(list.contains(hi));
	assertFalse(list.contains(nullString));
	}

	@Test
	public void addTest() {
	assertTrue(list.add("A"));
	assertEquals(1, list.size());
	assertTrue(list.add(null));
	assertEquals(2, list.size());
	assertTrue(list.add(" "));
	}

	@Test
	public void removeTest() {
	assertFalse(list.remove(null));
	assertFalse(list.remove(" �;;+������"));
	String hello = "Hello";
	list.add(hello);
	assertTrue(list.remove(hello));

	}

	@Test
	public void addAllTest() {
	List<String> collection;
	collection = new ArrayList<>();
	collection.add("���j�");
	assertTrue(list.addAll(collection));
	}

	@Test
	public void clearAndSizeTest() {
	final int BIG_LIST_SIZE = 10_000;
	for (int i = 0; i < BIG_LIST_SIZE; i++) {
	list.add(String.valueOf(i));

	}
	assertEquals(BIG_LIST_SIZE, list.size());

	list.clear();
	assertEquals(0, list.size());
	}

	@Test
	public void removeAllTest() {
	List<String> collection;
	collection = new ArrayList<>();
	collection.add("���j�");
	String hello = "Hello";
	list.add(hello);
	assertFalse(list.removeAll(collection));
	collection.add(hello);
	assertTrue(list.removeAll(collection));

	}

	@Test
	public void retainAllTest() {
	List<String> collection;
	collection = new ArrayList<>();
	collection.add("���j�");
	list.add("hello");
	assertTrue(list.retainAll(collection));
	collection.add("hello");
	assertFalse(list.retainAll(collection));

	}
	@Test
	public void SortTest() {
	list.add("hello");
	list.add("ahoj");
	list.add("@@");
	list.add("!!!!");
	//list.sort();

	}

	@Test
	public void addDuplicateValue(){
		list.add(S1);
		list.add(S2);
		list.add(S3);
		list.add(S4_2);
		
		assertEquals(4, list.size());
		assertSame(S2, list.get(1));
	}
	
	@Test
	public void addAcceptNullValues(){
		list.add(S1);
		list.add(null);
		list.add(S3);
		list.add(null);
		
		assertEquals(4, list.size());
		assertSame(S1, list.get(0));
		assertSame(S3, list.get(2));
		assertSame(null, list.get(3));
	}
}