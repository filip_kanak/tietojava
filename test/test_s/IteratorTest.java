package test_s;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.ListIterator;

import org.junit.Before;
import org.junit.Test;

public class IteratorTest extends ListTest {
	
	@Before
	public void initList() {
		list.add(S1);
		list.add(S2);
		list.add(S3);
		list.add(S4_2);
		assertEquals(4, list.size());
	}
	
	@Test
	public void basicTest() {
		
		final Iterator<String> iterator = list.iterator();
		
		int index = 0;
		while (iterator.hasNext()) {
			final String element = iterator.next();
			assertSame(list.get(index),element);
			index++;
		}
	}
	
	@Test
	public void basicListTest() {
		final ListIterator<String> iterator = list.listIterator();
		assertFalse(iterator.hasPrevious());
		
		int index = 0;
		while (iterator.hasNext()) {
			assertEquals(index-1, iterator.previousIndex());
			assertEquals(index, iterator.nextIndex());
			final String element = iterator.next();
			assertEquals(index, iterator.previousIndex());
			assertEquals(index+1, iterator.nextIndex());
			
			assertSame(list.get(index), element);
			
			index++;
		}
		assertFalse(iterator.hasNext());
		
		while (iterator.hasPrevious()) {
			index--;
			assertEquals(index, iterator.previousIndex());
			assertEquals(index+1, iterator.nextIndex());
			final String element = iterator.previous();
			assertEquals(index-1, iterator.previousIndex());
			assertEquals(index, iterator.nextIndex());
			
			assertSame(list.get(index), element);
		}
		assertFalse(iterator.hasPrevious());
	}
	
	@Test
	public void iteratorAdd() {
		final ListIterator<String> iterator = list.listIterator();
		iterator.next();
		iterator.next();
		
		assertEquals(1, iterator.previousIndex());
		assertEquals(2, iterator.nextIndex());
		assertEquals(4, list.size());
		iterator.add(S5);
		
		assertEquals(5, list.size());
		assertEquals(2, iterator.previousIndex());
		assertEquals(3, iterator.nextIndex());
		assertSame(S5, iterator.previous());
		assertSame(S5, iterator.next());
		assertSame(S3, iterator.next());
	}
	
	@Test
	public void iteratorRemove() {
		final ListIterator<String> iterator = list.listIterator();
		
		assertEquals(4, list.size());
		
	}
}
