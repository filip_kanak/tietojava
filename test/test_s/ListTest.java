package test_s;

import java.util.ArrayList;
import java.util.List;


import org.junit.Before;

public abstract class ListTest {
	protected static final String S1 = "Jan";
	protected static final String S2 = "Filip";
	protected static final String S3 = "Adam";
	protected static final String S4_2 = "Filip";
	protected static final String S5 = "Alena";
	
	protected List<String> list;

	@Before
	public void setUp() throws Exception {
	list = new ArrayList<>();
	}
}