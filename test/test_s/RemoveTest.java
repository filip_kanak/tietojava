package test_s;

import static org.junit.Assert.*;

import org.junit.Test;

public class RemoveTest extends ListTest {

	@Test
	public void basicRemove() {
		list.add(S1);
		list.add(S2);
		list.add(S3);
		list.add(S4_2);
		assertEquals(4, list.size());

		// Now call the tasted method
		list.remove(1);

		// Verify the result
		assertEquals(3, list.size());
		assertSame(S1, list.get(0));
		assertSame(S3, list.get(1));
		assertSame(S4_2, list.get(2));
	}

	@Test
	public void multipleRemoves() {
		list.add(S1);
		list.add(S2);
		list.add(S3);
		list.add(S4_2);

		assertEquals(4, list.size());

		list.remove(0);
		list.remove(2);
		assertEquals(2, list.size());

		assertSame(S2, list.get(0));
		assertSame(S3, list.get(1));

	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void removeJustBehindSize() {
		removeOnIndex(3);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void removeBehindSize() {
		removeOnIndex(100);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void removeJustBeforeZero() {
		removeOnIndex(-1);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void removeBeforeZero() {
		removeOnIndex(-100);
	}

	private void removeOnIndex(int index) {
		list.add(S1); // 1
		list.add(S2); // 2
		list.add(S3); // 2

		list.remove(index);
	}

}
