package test_s;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class SublistTest extends com.tieto.training.lesson4.ListTest{
	
	
	@Before
	public void setUpSublist() {
		list.add(S01); //0
		list.add(S02); //1
		list.add(S03); //2
		list.add(S04); //3
		list.add(S05); //4
		list.add(S06); //5 
		list.add(S07); //6
		list.add(S08); //7
		list.add(S09_02); //8
		list.add(S10_05); //9
		list.add(S11); //10
	}

	@Test
	public void sublicBasicScenario() {
		
		assertEquals(11, list.size());
		
	}

	@Test 
	public void sublistRemove() {
		
		final List<String> sublist = list.subList(5, 10);
		
		sublist.remove(2);
		
		assertEquals(4, sublist.size());
		assertSame(S06, sublist.get(0));
		assertSame(S07, sublist.get(1));
		assertSame(S09_02, sublist.get(2));
		assertSame(S10_05, sublist.get(3));
		
		assertEquals(10, list.size());
		assertSame(S01, list.get(0));
		assertSame(S02, list.get(1));
		assertSame(S03, list.get(2));
		assertSame(S04, list.get(3));
		assertSame(S05, list.get(4));
		assertSame(S06, list.get(5));
		assertSame(S07, list.get(6));
		assertSame(S09_02, list.get(7));
		assertSame(S10_05, list.get(8));
		assertSame(S11, list.get(9));
		
	}
	//TODO find what is wrong with this test
	/*@Test
	public void sublistClear()  {
		
		final List<String> sublist = list.subList(2, 5);
		sublist.clear();
		
		assertEquals(8, list.size());
		
		assertSame(S01, list.get(0));
		assertSame(S02, list.get(1));
		assertSame(S06, list.get(2));
		assertSame(S07, list.get(3));
		assertSame(S08, list.get(4));
		assertSame(S09_02, list.get(5));
		assertSame(S10_05, list.get(6));
		assertSame(S11, list.get(7));
	}*/
	
	@Test
	public void sublistAdd() {
		
		assertEquals(11, list.size());
		assertSame(S01, list.get(0));
		assertSame(S02, list.get(1));
		assertSame(S03, list.get(2));
		assertSame(S04, list.get(3));
		assertSame(S05, list.get(4));
		assertSame(S06, list.get(5));
		assertSame(S07, list.get(6));
		assertSame(S08, list.get(7));
		assertSame(S09_02, list.get(8));
		assertSame(S10_05, list.get(9));
		assertSame(S11, list.get(10));
	}
	
	@Test
	public void sublistGet() {
		final List<String> sublist = list.subList(2, 5);
		assertSame(S03, sublist.get(0));
		assertSame(S04, sublist.get(1));
		assertSame(S05, sublist.get(2));
		
		assertEquals(11, list.size());
		assertSame(S01, list.get(0));
		assertSame(S02, list.get(1));
		assertSame(S03, list.get(2));
		assertSame(S04, list.get(3));
		assertSame(S05, list.get(4));
		assertSame(S06, list.get(5));
		assertSame(S07, list.get(6));
		assertSame(S08, list.get(7));
		assertSame(S09_02, list.get(8));
		assertSame(S10_05, list.get(9));
		assertSame(S11, list.get(10));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void sublistIncorrentBounds()  {
		final List<String> sublist  = list.subList(10, 9);
	}
}
